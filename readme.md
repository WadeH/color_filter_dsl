# Color Filter DSL

__Ruby DSL for Image Color Filtering__

## Overview

The intent of this project is to develop a DSL that can be used to describe color filters for modifying images on a per-pixel basis. Kind of like a very dumbed down version of Adobe PixelBender.


## About

Developed and tested with Ruby 2.2.3.

Released under MIT license.

(c) 2017, Wade H. vdtdev@gmail.com