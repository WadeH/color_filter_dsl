require "chunky_png"

module CFilter
  module Support
  
    class Color

      HEX_PARTS = /(?<r>[a-f0-9]{2})(?<g>[a-f0-9]{2})(?<b>[a-f0-9]{2})(?<a>[a-f0-9]{2})*/

      attr_accessor :r, :g, :b, :a

      def initialize(r,g,b,a=1.0)
        @r=r
        @g=g
        @b=b
        @a=a
      end

      def self.from_hex(hex)
        hex = ('%-8.8s' % hex).gsub(" ", "0")
        parts = Color::HEX_PARTS.match(hex)
        cap_by_name = Proc.new do |match, name|
          v=match.captures[match.names.index(name.to_s)]
          v = v.to_i(16) unless v.nil?
        end
        r = cap_by_name.(parts, :r)
        g = cap_by_name.(parts, :g)
        b = cap_by_name.(parts, :b)
        a = cap_by_name.(parts, :a)
        color = self.new(r,g,b,a)
        color
      end

      def self.from_int(code)
        self.from_hex(code.to_s(16))
      end

      def self.from_h(hash)
        r = hash.fetch(:r, 0)
        g = hash.fetch(:g, 0)
        b = hash.fetch(:b, 0)
        a = hash.fetch(:a, nil)
        return self.new(r,g,b,a)
      end


      def as_bytes()
        @r = (@r * 255).to_i if @r.is_a?(Float)
        @g = (@g * 255).to_i if @g.is_a?(Float)
        @b = (@b * 255).to_i if @b.is_a?(Float)
        @a = (@a * 255).to_i if @a.is_a?(Float)
        self
      end

      def as_floats()
        @r = (@r / 255.0).to_f if @r.is_a?(Integer)
        @g = (@g / 255.0).to_f if @g.is_a?(Integer)
        @b = (@b / 255.0).to_f if @b.is_a?(Integer)
        @a = (@a / 255.0).to_f if @a.is_a?(Integer)
        self
      end

      def to_h(channels=[:r,:g,:b,:a])
        h = {r: @r, g: @g, b: @b, a: @a}
        h = h.select{|k,v| channels.include?(k) }
        h
      end

      def as_chunky()
        @a = 1.0 if @a.nil?
        self.as_bytes
        ChunkyPNG::Color.rgba(@r,@g,@b,@a)
      end


    end

  end
end