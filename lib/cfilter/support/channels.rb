module CFilter
  module Support
    ##
    # Channel processing helper method module
    #
    module Channels

        ##
        # Splits channel combo symbols and strings into channel arrays
        # @param [Symbol|String] channels The channel or combined channels to split
        # @return [Array] array containing split channel symbols
        #
        def self.split(channels)
            if channels.is_a?(Symbol) or channels.is_a?(String)
                return channels.to_s.split('').map{|s|s.to_sym}
            else
                return channels
            end
        end

    end
  end
end
