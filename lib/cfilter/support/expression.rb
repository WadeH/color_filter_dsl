require_relative "channels.rb"

module CFilter
  module Support
    ##
    # Class providing implementation of filter pass operations
    # and a context in which to calculate results of a pass operation
    #
    class Expression

        attr_accessor :storage,:source,:out

        ##
        # Class constructor for Expression
        # @param [Hash] store Hash containing starting channel values
        # @param [Hash] io IO optional parameters hash
        # @option io [Array] :source Pass source channels, default is empty
        # @option io [Array] :out Pass output channels, targetted when `output` operation is used; default is empty
        #
        def initialize(store,io={})
            @storage = store
            @source=io.fetch(:source,[])
            @out=io.fetch(:out,[])
        end

        ##
        # Determines if a > b
        # @param [Symbol|Numeric] a First channel name or constant 
        # @param [Symbol|Numeric] b Second channel name or constant
        # @param [Proc] true_l Proc to execute if a > b
        # @param [Proc] false_l Proc to execute if a <= b
        # @return [Numeric] result of called proc chosen by evaluating condition
        #
        def above(a,b,true_l,false_l)
            return true_l.() if resolve(a) > resolve(b)
            return false_l.()
        end

        ##
        # Determines if a < b
        # @param [Symbol|Numeric] a First channel name or constant 
        # @param [Symbol|Numeric] b Second channel name or constant
        # @param [Proc] true_l Proc to execute if a < b
        # @param [Proc] false_l Proc to execute if a >= b
        # @return [Numeric] result of called proc chosen by evaluating condition
        #
        def below(a,b,true_l,false_l)
            return true_l.() if resolve(a) < resolve(b)
            return false_l.()
        end

        ##
        # Multiplies a * b
        # @param [Symbol|Numeric] a First channel name or constant 
        # @param [Symbol|Numeric] b Second channel name or constant
        # @return [Numeric] product of a and b
        #
        def product(a,b)
            resolve(a)*resolve(b)
        end

        ##
        # Adds a + b
        # @param [Symbol|Numeric] a First channel name or constant 
        # @param [Symbol|Numeric] b Second channel name or constant
        # @return [Numeric] sum of a and b
        #
        def sum(a,b)
            resolve(a)+resolve(b)
        end

        def min(a,b)
            [resolve(a),resolve(b)].min
        end

        def max(a,b)
            [resolve(a),resolve(b)].max
        end

        def result(&block)
            ->{yield}
        end

        def resolve(channel)
            if channel.is_a?(Symbol) 
                return @storage[channel]
            else
                return channel
            end
        end

        def store(channel, value)
            channels = CFilter::Support::Channels::split(channel)
            channels.each do |ch|
              @storage[ch] = resolve(value)
            end
        end

        ##
        # Call store on value using all channels provided in constructor out parameter
        def output(value)
            @out.each do |o|
                store(o, resolve(value))
            end
        end

        def execute(proc)
            self.instance_eval(&proc)
        end

    end
  end
end
