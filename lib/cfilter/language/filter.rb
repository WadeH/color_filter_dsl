require_relative "blocks.rb"
require_relative "../support/channels.rb"
require_relative "../support/expression.rb"
module CFilter
    module Language
        class Filter < CFilter::Language::Blocks::DefinableBlock

            attr_reader :metadata, :channel_data, :passes

            def initialize()
                @metadata = nil
                @channel_data = nil
                @passes = []
            end

            def meta(&block)
                @metadata = Meta.define(&block)
            end

            def data(&block)
                @channel_data = Data.define(&block)
            end

            def pass(&block)
                @passes << Pass.define(&block)
            end

            ##
            # Class defining 'meta' block in filters
            class Meta < CFilter::Language::Blocks::DefinableBlock

                attr_accessor :name, :author, :description

                def initialize()
                    @name = nil
                    @author = nil
                    @description = nil
                end

                def name(name_val)
                    @name = name_val
                end

                def author(auth_val)
                    @author = auth_val
                end

                def description(desc_val)
                    @description = desc_val
                end

            end

            ##
            # Class defining the'data' block in a filter
            class Data < CFilter::Language::Blocks::DefinableBlock

                attr_accessor :inputs, :outputs

                def initialize()
                    @inputs = []
                    @outputs = []
                end

                def input(channels)
                    @inputs = CFilter::Support::Channels::split(channels)
                end

                def output(channels)
                    @outputs = CFilter::Support::Channels::split(channels)
                end

            end

            ##
            # Class defining 'pass' blocks in a filter
            class Pass < CFilter::Language::Blocks::DefinableBlock

                attr_accessor :inputs, :outputs, :result

                def initialize()
                    @inputs = []
                    @outputs = []
                    @result = nil
                end

                def source(channels)
                    @inputs = CFilter::Support::Channels::split(channels)
                end

                def out(channels)
                    @outputs = CFilter::Support::Channels::split(channels)
                end

                # def store(proc)
                #     @result = Exec.new(proc)
                # end

                def store(&block)
                    @result = Exec.new(block, source: @inputs, out: @outputs)
                end

            end

            class Exec < CFilter::Language::Blocks::DefinableBlock

                attr_accessor :executable, :io

                def initialize(proc, io)
                    @executable = proc
                    @io = { source: io.fetch(:source, []), out: io.fetch(:out, []) }
                end

                def execute(channel_storage)
                    @ex = CFilter::Support::Expression.new(channel_storage, @io)
                    @ex.execute(@executable)
                    @ex.storage
                end

            end

        end
    end
end
