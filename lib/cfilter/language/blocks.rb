module CFilter
  module Language
    ##
    # DSL Block base classes
    #
    module Blocks

        ##
        # Base class for classes describing blocks opened with Name.define
        #
        class DefinableBlock

            def self.define(&block)
                dblock = self.new
                dblock.instance_eval(&block)
                dblock
            end

        end

    end
  end
end