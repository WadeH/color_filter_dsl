require_relative "../language/filter.rb"
require_relative "../support/color.rb"

module CFilter
  module Runner

    ##
    # Class for pass runner
    # @author Wade H (vdtdev@gmail.com)
    #
    class PassRunner

      attr_accessor :data

      def initialize(filter)
        @filter = filter
        @data = {
          input: [],
          output: []
        }
      end

      ##
      # Clear input and output data
      #
      def reset()
        @data[:input] = []
        @data[:output] = []
      end

      def load_input(input_data)
        @data[:input] = input_data
      end

      def run_passes()

        src_data = @data[:input].dup
        src_data.each do |dat|

          cdat = CFilter::Support::Color.from_h(dat)
          cdat.as_floats

          pass_outputs = []
          @filter.passes.each_with_index do |pass,idx|

            pass_out = pass.result.execute(cdat.to_h)
            if pass.outputs != []
              pass_out = pass_out.select{|k,v| pass.outputs.include?(k)}
            end

            pass_outputs << pass_out

          end

          combined = {}
          pass_outputs.each do |out|
            combined = combined.merge(out)
          end

          cout = CFilter::Support::Color.from_h(combined)
          cout.as_bytes
          @data[:output] << cout.to_h(@filter.channel_data.outputs)

        end

        # binding.pry
        # @data[:input].each do |dat|
        #   pass_stores = []
        #   @filter.passes.each_with_index do |pass,i|
        #     r = pass.result.execute(dat)
        #     if pass.outputs != []
        #       # limit saved results of this pass to output channels, if set
        #       r = r.select{|k,v| pass.outputs.include?(k) }
        #     end
        #     pass_stores << r
        #   end
        #   color = {}
        #   pass_stores.each do |pass|
        #     color = color.merge(pass)
        #   end
        #   c = CFilter::Support::Color.from_h(color)
        #   c.as_bytes
        #   @data[:output] << c.to_h
        # end

      end

    end

  end
end