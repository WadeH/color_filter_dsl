require "chunky_png"
require_relative "../../support/color.rb"

module CFilter
  module Runner
    module ImageHandler

      class Reader

        attr_accessor :image, :data, :image_info

        def initialize(filename, channels=[])
          @settings={
            source: filename,
            channels: channels
          }
          @image = nil
          @image_info = {}
          @data = []
        end

        def read_colors()
          load_image
          tmp_colors = []
          xmax = (@image_info[:width] - 1)
          ymax = (@image_info[:height] - 1)
          (0..ymax).each do |y|
            (0..xmax).each do |x|
              c = CFilter::Support::Color.from_int(@image[x,y])
              c.as_bytes
              tmp_colors << c.to_h(@settings[:channels])
            end
          end
          @data = tmp_colors
        end

        def color_test(color,channels)
          color_channels(color,channels)
        end

        private
        def load_image()
          @image = ChunkyPNG::Image.from_file(@settings[:source])
          @image_info = {width: @image.width, height: @image.height}
        end

        def color_hex(code)
          code.to_s(16)
        end

        def color_channels(hex, channels)
          parts = /(?<r>[a-f0-9]{2})(?<g>[a-f0-9]{2})(?<b>[a-f0-9]{2})(?<a>[a-f0-9]{2})*/.match(hex)
          color = {}
          channels.each do |channel|
            if not parts.nil?
              i = parts.names.index(channel.to_s)
              color[channel] = parts.captures[i].to_i(16)
            else
              color[channels] = 0
            end
          end
          color
        end



      end

    end
  end
end