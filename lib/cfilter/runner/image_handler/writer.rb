require "chunky_png"
require_relative "../../support/color.rb"

module CFilter
  module Runner
    module ImageHandler

      class Writer

        attr_accessor :image_info, :data

        def initialize(data, image_params={})
          @data = data
          @image_info = {
            filename: image_params.fetch(:filename, nil),
            width: image_params.fetch(:width, 0),
            height: image_params.fetch(:height, 0)
          }
        end

        def write_image()
          image = ChunkyPNG::Image.new(
            @image_info[:width], @image_info[:height], 
            ChunkyPNG::Color::TRANSPARENT
          )

          xmax = @image_info[:width] - 1
          ymax = @image_info[:height] - 1
          counter = 0
          (0..ymax).each do |y|
            (0..xmax).each do |x|

              color = CFilter::Support::Color.from_h(@data[counter])
              color.as_bytes
              color = color.as_chunky

              image[x,y] = color

              counter += 1

            end
          end

          image.save(@image_info[:filename])

        end

      end

    end
  end
end