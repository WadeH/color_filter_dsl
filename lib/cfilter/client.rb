
require_relative "runner/pass_runner.rb"
require_relative "runner/image_handler/reader.rb"
require_relative "runner/image_handler/writer.rb"
require_relative "language/filter.rb"

require "chunky_png"

module CFilter

  ##
  # Primary Client class for CFilter
  #
  class Client

    attr_accessor :image, :filter, :runners, :data

    def initialize(options={})
      @image = nil
      @filter = nil
      @settings = {
        channels: options.fetch(:channels, [])
      }
      @data = {
        source: [],
        output: []
      }
      @runners = {
        image_reader: nil,
        image_writer: nil,
        pass: nil
      }
    end

    ##
    # Reset data and parameters
    # @param [Hash] options Optional parameter hash
    # @option options [Boolean] :filter If false, skip clearing filter. Default true
    # @option options [Boolean] :image_reader If false, skip clearing image reader. Default true
    # @option options [Boolean] :image_writer If false, skip clearing image writer. Default true
    #
    def reset(options={})
      @image = nil
      @filter = nil if options.fetch(:filter, true)
      @runners[:image_reader] = nil if options.fetch(:image_reader, true)
      @runners[:image_writer] = nil if options.fetch(:image_writer, true)
      @data={source: [], output: []}
    end

    def set_filter(filter)
      @filter = filter
      @runners[:pass] = CFilter::Runner::PassRunner.new(@filter)
    end

    def set_image(filename)
      # binding.pry
      @image = filename
      @runners[:image_reader] = CFilter::Runner::ImageHandler::Reader.new(filename, resolve_channels)
    end

    def run_filter_passes()
      raise "No filter loaded" if @filter.nil?
      raise "No pass runner created" if @runners[:pass].nil?

      @runners[:pass].load_input(@data[:source].dup)
      @runners[:pass].run_passes

      @data[:output] = @runners[:pass].data[:output]
    end

    def load_input_from_image(limit=nil)
      raise "No image loaded" if @image.nil?
      raise "No image reader created" if @runners[:image_reader].nil?

      @runners[:image_reader].read_colors()

      @data[:source] = (limit.nil?)? @runners[:image_reader].data : @runners[:image_reader].data[0..limit]
    end

    def write_output_image(filename, width, height)
      raise "No output data available" if @data[:output] == []

      @runners[:image_writer] =  
        CFilter::Runner::ImageHandler::Writer.new(
          @data[:output], filename: filename, width: width, height: height
        )

      @runners[:image_writer].write_image()
    end
    
    private

    def resolve_channels()
      # binding.pry
      if not @filter.nil?
        return @filter.channel_data.outputs
      else
        return @settings[:channels]
      end
    end

  end

end