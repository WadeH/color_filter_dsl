require 'lib_helper'

RSpec.describe CFilter::Client do
  
  context "Single-pass filter on 64x64 image" do
    before :each do
      
      Filter = CFilter::Language::Filter
      @filter = Filter.define do
        meta do
          name "Highest Channel Filter"
          description "Zeros out all channels except the one with the highest value"
        end
        data do
          input :rgb
          output :rgb
        end
        pass do
          source :rgb
          out :rgb
          store do
            above :r, :g, result {
              above :r, :b, result {
                store :gb, 0.0
              }, result {
                store :rg, 0.0; 
              }
            }, result {
              above :g, :b, result {
                store :rb, 0.0
              }, result {
                store :rb, 0.0
              }
            }
          end
        end
      end

      @client = CFilter::Client.new

      @client1_step = Proc.new do |step|
        case step
        when :image
          @client.set_image("samp2.png")
        when :filter
          @client.set_filter(@filter)
        when :reset_all
          @client.reset(filter: false, image_reader: false, image_writer: false)
        when :reset_default
          @client.reset()
        end
      end

    end
    
    it "Creates image reader on setting image" do
      @client1_step.(:image)
      expect(@client.image).to_not be(nil)
      expect(@client.runners[:image_reader]).to_not be(nil)
    end

    it "Creates pass runner on setting filter" do
      @client1_step.(:image)
      @client1_step.(:filter)
      expect(@client.filter).to_not be(nil)
      expect(@client.runners[:pass]).to_not be(nil)
    end
  end

end