require 'lib_helper'

RSpec.describe CFilter::Language::Filter do
  context "Filter with single pass" do
    
    before :each do
      @filter = CFilter::Language::Filter.define do
        meta do
          name "Single Pass Filter"
        end
        data do
          input :rgb
          output :rgb
        end
        pass do
          source :rgb
          out :g
          store do
            store :g, (min 1.0, (max :g, (sum :r, :b)))
          end
        end
      end
    end

    it "contains correct number of passes" do
      expect(@filter.passes.length).to eq(1)
    end

    it "outputs expected value" do
      a = @filter.passes[0].result.execute({r: 0.4, g: 0.7, b: 0.2})
      expect(a[:g]).to eq(0.7)
    end

  end
end